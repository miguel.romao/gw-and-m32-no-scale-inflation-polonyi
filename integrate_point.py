import math
import numpy as np
import scipy.integrate as integrate
from sympy.parsing.mathematica import parse_mathematica
from sympy import var, lambdify


import warnings

warnings.filterwarnings("ignore", category=RuntimeWarning)
warnings.filterwarnings("ignore", category=UserWarning)


musol = "0.00006631915043956543*Sqrt[((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^8*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))^3*(-((E^(12*b*phi^2)*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi))^2*(8*b*E^(4*b*phi^2)*phi - 48*a*phi^2*(32*b^2*phi^3 + 4*b*phi*(-9 + 8*b*phi^2)) -96*a*phi*(3 + 2*b*phi^2*(-9 + 8*b*phi^2))))/((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))^2)) +(2*E^(12*b*phi^2)*(Sqrt[3]*Sqrt[c] - 6*l*phi)*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi)))/((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))) + (24*b*E^(12*b*phi^2)*phi*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi))^2)/((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))) -(2*E^(12*b*phi^2)*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi))^2*(2*E^(4*b*phi^2)*phi - 192*a*phi^3 + 8*b*E^(4*b*phi^2)*phi*(-3*c + phi^2)))/((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^3*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))))^2)/(E^(36*b*phi^2)*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi))^6*(c*E^(8*b*phi^2) + 768*a^2*phi^6*(1 + 2*b*phi^2) +16*a*E^(4*b*phi^2)*phi^2*(2*b*phi^4*(-5 + 8*b*phi^2) + c*(-9 + 6*b*phi^2*(9 - 8*b*phi^2)))))]"

Kphiphi = "(3*(phi^2/9 + (32*a*phi^4*(-2 + 4*b*phi^2))/(3*E^(4*b*phi^2)) + (256*a^2*phi^6*(-2 + 4*b*phi^2)^2)/E^(8*b*phi^2) -(c - phi^2/3 + (16*a*phi^4)/E^(4*b*phi^2))*(-1/3 + (8*a*phi^2*(6 + 4*b*phi^2*(-9 + 8*b*phi^2)))/E^(4*b*phi^2))))/(c - phi^2/3 + (16*a*phi^4)/E^(4*b*phi^2))^2"

dxLnV = "((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2*(E^(4*b*phi^2) - 24*a*phi^2*(6 - 36*b*phi^2 + 32*b^2*phi^4))*((-9*E^(12*b*phi^2)*(d*mu + phi*((Sqrt[c]*mu)/Sqrt[3] - l*mu*phi))^2*(8*b*E^(4*b*phi^2)*phi - 24*a*phi^2*(-72*b*phi + 128*b^2*phi^3) - 48*a*phi*(6 - 36*b*phi^2 + 32*b^2*phi^4)))/((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2*(E^(4*b*phi^2) - 24*a*phi^2*(6 - 36*b*phi^2 + 32*b^2*phi^4))^2) +(18*E^(12*b*phi^2)*((Sqrt[c]*mu)/Sqrt[3] - 2*l*mu*phi)*(d*mu + phi*((Sqrt[c]*mu)/Sqrt[3] - l*mu*phi)))/((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2* (E^(4*b*phi^2) - 24*a*phi^2*(6 - 36*b*phi^2 + 32*b^2*phi^4))) + (216*b*E^(12*b*phi^2)*phi*(d*mu + phi*((Sqrt[c]*mu)/Sqrt[3] - l*mu*phi))^2)/((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2*(E^(4*b*phi^2) - 24*a*phi^2*(6 - 36*b*phi^2 + 32*b^2*phi^4))) -(18*E^(12*b*phi^2)*(d*mu + phi*((Sqrt[c]*mu)/Sqrt[3] - l*mu*phi))^2*(2*E^(4*b*phi^2)*phi - 192*a*phi^3 + 8*b*E^(4*b*phi^2)*phi*(-3*c + phi^2)))/((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^3*(E^(4*b*phi^2) - 24*a*phi^2*(6 - 36*b*phi^2 + 32*b^2*phi^4)))))/ (27*Sqrt[2]*E^(12*b*phi^2)*(d*mu + phi*((Sqrt[c]*mu)/Sqrt[3] - l*mu*phi))^2*Sqrt[(c*E^(8*b*phi^2) + 768*a^2*phi^6*(1 + 2*b*phi^2) + 16*a*E^(4*b*phi^2)*phi^2*(2*b*phi^4*(-5 + 8*b*phi^2) + c*(-9 + 6*b*phi^2*(9 - 8*b*phi^2))))/(-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2])"

dxic = "-1/3*(Sqrt[2]*Abs[(3*Sqrt[3]*c^(3/2)*E^(4*b*phi^2)*(E^(4*b*phi^2) - 96*a*b*phi^4*(15 - 52*b*phi^2 + 32*b^2*phi^4)) +Sqrt[3]*Sqrt[c]*phi^2*(E^(8*b*phi^2) - 13824*a^2*phi^4*(-2 + 21*b*phi^2 - 52*b^2*phi^4 + 32*b^3*phi^6) +48*a*E^(4*b*phi^2)*phi^2*(-9 + 74*b*phi^2 - 136*b^2*phi^4 +64*b^3*phi^6)) - 18*c*E^(4*b*phi^2)*phi*(E^(4*b*phi^2)*l + 24*a*(d*(-3 + 48*b*phi^2 - 120*b^2*phi^4 + 64*b^3*phi^6) -l*phi^2*(3 + 12*b*phi^2 - 88*b^2*phi^4 + 64*b^3*phi^6))) +6*(d*phi*(E^(8*b*phi^2) + 24*a*E^(4*b*phi^2)*phi^2*(-13 + 92*b*phi^2 - 152*b^2*phi^4 + 64*b^3*phi^6) - 1152*a^2*phi^4*(-15 + 144*b*phi^2 - 328*b^2*phi^4 + 192*b^3*phi^6)) +24*a*l*phi^5*(E^(4*b*phi^2)*(5 - 56*b*phi^2 + 120*b^2*phi^4 -64*b^3*phi^6) + 48*a*phi^2*(-9 + 108*b*phi^2 - 296*b^2*phi^4 +192*b^3*phi^6))))/((3*c*E^(4*b*phi^2) - E^(4*b*phi^2)*phi^2 + 48*a*phi^4)*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi))* (E^(4*b*phi^2) - 48*a*phi^2*(3 - 18*b*phi^2 + 16*b^2*phi^4))* Sqrt[(c*E^(8*b*phi^2) + 768*a^2*phi^6*(1 + 2*b*phi^2) + 16*a*E^(4*b*phi^2)*phi^2*(2*b*phi^4*(-5 + 8*b*phi^2) + c*(-9 + 6*b*phi^2*(9 - 8*b*phi^2))))/ (-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2])])"

V = "(9*E^(12*b*phi^2)*(d*mu + phi*((Sqrt[c]*mu)/Sqrt[3] - l*mu*phi))^2)/((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2*(E^(4*b*phi^2) - 24*a*phi^2*(6 - 36*b*phi^2 + 32*b^2*phi^4)))"

r = "(4*(-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^6*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))^2*(-((E^(12*b*phi^2)*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi))^2*(8*b*E^(4*b*phi^2)*phi - 48*a*phi^2*(32*b^2*phi^3 + 4*b*phi*(-9 + 8*b*phi^2)) - 96*a*phi*(3 + 2*b*phi^2*(-9 + 8*b*phi^2))))/((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))^2)) + (2*E^(12*b*phi^2)*(Sqrt[3]*Sqrt[c] - 6*l*phi)*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi)))/((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))) + (24*b*E^(12*b*phi^2)*phi*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi))^2)/ ((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))) - (2*E^(12*b*phi^2)*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi))^2*(2*E^(4*b*phi^2)*phi - 192*a*phi^3 + 8*b*E^(4*b*phi^2)*phi*(-3*c + phi^2)))/ ((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^3*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))))^2)/ (9*E^(24*b*phi^2)*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi))^4*(c*E^(8*b*phi^2) + 768*a^2*phi^6*(1 + 2*b*phi^2) +16*a*E^(4*b*phi^2)*phi^2*(2*b*phi^4*(-5 + 8*b*phi^2) + c*(-9 + 6*b*phi^2*(9 - 8*b*phi^2)))))"

ns = "1 - ((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^6*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))^2*(-((E^(12*b*phi^2)*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi))^2*(8*b*E^(4*b*phi^2)*phi - 48*a*phi^2*(32*b^2*phi^3 + 4*b*phi*(-9 + 8*b*phi^2)) - 96*a*phi*(3 + 2*b*phi^2*(-9 + 8*b*phi^2))))/((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))^2)) +(2*E^(12*b*phi^2)*(Sqrt[3]*Sqrt[c] - 6*l*phi)*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi)))/((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2* (E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))) + (24*b*E^(12*b*phi^2)*phi*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi))^2)/((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))) -(2*E^(12*b*phi^2)*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi))^2*(2*E^(4*b*phi^2)*phi - 192*a*phi^3 + 8*b*E^(4*b*phi^2)*phi*(-3*c + phi^2)))/((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^3*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))))^2)/(6*E^(24*b*phi^2)*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi))^4*(c*E^(8*b*phi^2) + 768*a^2*phi^6*(1 + 2*b*phi^2) +16*a*E^(4*b*phi^2)*phi^2*(2*b*phi^4*(-5 + 8*b*phi^2) + c*(-9 + 6*b*phi^2*(9 - 8*b*phi^2))))) +(Sqrt[2]*(-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))*(((2*E^(12*b*phi^2)*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi))^2*(8*b*E^(4*b*phi^2)*phi - 48*a*phi^2*(32*b^2*phi^3 + 4*b*phi*(-9 + 8*b*phi^2)) -96*a*phi*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))^2)/((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))^3) - (E^(12*b*phi^2)*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi))^2*(8*b*E^(4*b*phi^2) + 64*b^2*E^(4*b*phi^2)*phi^2 - 48*a*phi^2*(160*b^2*phi^2 + 4*b*(-9 + 8*b*phi^2)) - 192*a*phi*(32*b^2*phi^3 + 4*b*phi*(-9 + 8*b*phi^2)) - 96*a*(3 + 2*b*phi^2*(-9 + 8*b*phi^2))))/ ((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))^2) - (4*E^(12*b*phi^2)*(Sqrt[3]*Sqrt[c] - 6*l*phi)*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi))*(8*b*E^(4*b*phi^2)*phi - 48*a*phi^2*(32*b^2*phi^3 + 4*b*phi*(-9 + 8*b*phi^2)) - 96*a*phi*(3 + 2*b*phi^2*(-9 + 8*b*phi^2))))/((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))^2) - (48*b*E^(12*b*phi^2)*phi*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi))^2*(8*b*E^(4*b*phi^2)*phi - 48*a*phi^2*(32*b^2*phi^3 + 4*b*phi*(-9 + 8*b*phi^2)) - 96*a*phi*(3 + 2*b*phi^2*(-9 + 8*b*phi^2))))/ ((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))^2) + (4*E^(12*b*phi^2)*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi))^2*(2*E^(4*b*phi^2)*phi - 192*a*phi^3 + 8*b*E^(4*b*phi^2)*phi*(-3*c + phi^2))*(8*b*E^(4*b*phi^2)*phi - 48*a*phi^2*(32*b^2*phi^3 + 4*b*phi*(-9 + 8*b*phi^2)) - 96*a*phi*(3 + 2*b*phi^2*(-9 + 8*b*phi^2))))/ ((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^3*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))^2) + (2*E^(12*b*phi^2)*(Sqrt[3]*Sqrt[c] - 6*l*phi)^2)/((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))) - (12*E^(12*b*phi^2)*l*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi)))/((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))) + (96*b*E^(12*b*phi^2)*phi*(Sqrt[3]*Sqrt[c] - 6*l*phi)*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi)))/ ((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))) + (24*b*E^(12*b*phi^2)*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi))^2)/((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))) + (576*b^2*E^(12*b*phi^2)*phi^2*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi))^2)/ ((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))) - (8*E^(12*b*phi^2)*(Sqrt[3]*Sqrt[c] - 6*l*phi)*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi))*(2*E^(4*b*phi^2)*phi - 192*a*phi^3 + 8*b*E^(4*b*phi^2)*phi*(-3*c + phi^2)))/ ((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^3*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))) - (96*b*E^(12*b*phi^2)*phi*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi))^2*(2*E^(4*b*phi^2)*phi - 192*a*phi^3 + 8*b*E^(4*b*phi^2)*phi*(-3*c + phi^2)))/ ((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^3*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))) + (6*E^(12*b*phi^2)*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi))^2*(2*E^(4*b*phi^2)*phi - 192*a*phi^3 + 8*b*E^(4*b*phi^2)*phi*(-3*c + phi^2))^2)/ ((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^4*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))) - (2*E^(12*b*phi^2)*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi))^2*(2*E^(4*b*phi^2) - 576*a*phi^2 + 32*b*E^(4*b*phi^2)*phi^2 + 8*b*E^(4*b*phi^2)*(-3*c + phi^2) + 64*b^2*E^(4*b*phi^2)*phi^2*(-3*c + phi^2)))/((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^3*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))))/(3*Sqrt[2]*Sqrt[(c*E^(8*b*phi^2) + 768*a^2*phi^6*(1 + 2*b*phi^2) + 16*a*E^(4*b*phi^2)*phi^2*(2*b*phi^4*(-5 + 8*b*phi^2) + c*(-9 + 6*b*phi^2*(9 - 8*b*phi^2))))/(-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2]) -((-((E^(12*b*phi^2)*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi))^2*(8*b*E^(4*b*phi^2)*phi - 48*a*phi^2*(32*b^2*phi^3 + 4*b*phi*(-9 + 8*b*phi^2)) -96*a*phi*(3 + 2*b*phi^2*(-9 + 8*b*phi^2))))/((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))^2)) + (2*E^(12*b*phi^2)*(Sqrt[3]*Sqrt[c] - 6*l*phi)*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi)))/((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))) + (24*b*E^(12*b*phi^2)*phi*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi))^2)/((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))) -(2*E^(12*b*phi^2)*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi))^2*(2*E^(4*b*phi^2)*phi - 192*a*phi^3 + 8*b*E^(4*b*phi^2)*phi*(-3*c + phi^2)))/((-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^3*(E^(4*b*phi^2) - 48*a*phi^2*(3 + 2*b*phi^2*(-9 + 8*b*phi^2)))))*((-2*(2*E^(4*b*phi^2)*phi - 192*a*phi^3 + 8*b*E^(4*b*phi^2)*phi*(-3*c + phi^2))*(c*E^(8*b*phi^2) + 768*a^2*phi^6*(1 + 2*b*phi^2) +16*a*E^(4*b*phi^2)*phi^2*(2*b*phi^4*(-5 + 8*b*phi^2) + c*(-9 + 6*b*phi^2*(9 - 8*b*phi^2)))))/(-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^3 +(16*b*c*E^(8*b*phi^2)*phi + 3072*a^2*b*phi^7 + 4608*a^2*phi^5*(1 + 2*b*phi^2) + 16*a*E^(4*b*phi^2)*phi^2*(32*b^2*phi^5 + 8*b*phi^3*(-5 + 8*b*phi^2) +c*(-96*b^2*phi^3 + 12*b*phi*(9 - 8*b*phi^2))) + 32*a*E^(4*b*phi^2)*phi*(2*b*phi^4*(-5 + 8*b*phi^2) + c*(-9 + 6*b*phi^2*(9 - 8*b*phi^2))) + 128*a*b*E^(4*b*phi^2)*phi^3*(2*b*phi^4*(-5 + 8*b*phi^2) + c*(-9 + 6*b*phi^2*(9 - 8*b*phi^2))))/(-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2))/(6*Sqrt[2]*((c*E^(8*b*phi^2) + 768*a^2*phi^6*(1 + 2*b*phi^2) + 16*a*E^(4*b*phi^2)*phi^2*(2*b*phi^4*(-5 + 8*b*phi^2) + c*(-9 + 6*b*phi^2*(9 - 8*b*phi^2))))/(-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2)^(3/2))))/(3*E^(12*b*phi^2)*(3*d + phi*(Sqrt[3]*Sqrt[c] - 3*l*phi))^2*Sqrt[(c*E^(8*b*phi^2) + 768*a^2*phi^6*(1 + 2*b*phi^2) + 16*a*E^(4*b*phi^2)*phi^2*(2*b*phi^4*(-5 + 8*b*phi^2) + c*(-9 + 6*b*phi^2*(9 - 8*b*phi^2))))/(-48*a*phi^4 + E^(4*b*phi^2)*(-3*c + phi^2))^2])"

m32 = "2.4*^18*Sqrt[((d*(Sqrt[3]*Sqrt[c] - Sqrt[3]*Sqrt[c + 4*d])*mu)/2 + (Sqrt[c]*(Sqrt[3]*Sqrt[c] - Sqrt[3]*Sqrt[c + 4*d])^2*mu)/(8*Sqrt[3]) - ((Sqrt[3]*Sqrt[c] - Sqrt[3]*Sqrt[c + 4*d])^3*l*mu)/24)^2/(c - (Sqrt[3]*Sqrt[c] - Sqrt[3]*Sqrt[c + 4*d])^2/12 + (a*(Sqrt[3]*Sqrt[c] - Sqrt[3]*Sqrt[c + 4*d])^4)/E^(b*(Sqrt[3]*Sqrt[c] - Sqrt[3]*Sqrt[c + 4*d])^2))^3]".replace(
    "*^", "*10^"
)

a = var("a")
b = var("b")
c = var("c")
d = var("d")
l = var("l")
phi = var("phi")
mu = var("mu")

func_musol = lambdify((a, b, c, d, l, phi), parse_mathematica(musol), modules="math")
func_dxLnV = lambdify((a, b, c, d, l, mu, phi), parse_mathematica(dxLnV), modules="math")
func_Kphiphi = lambdify((a, b, c, phi), parse_mathematica(Kphiphi), modules="math")
func_dxic = lambdify((a, b, c, d, l, mu, phi), parse_mathematica(dxic), modules="math")
func_V = lambdify((a, b, c, d, l, mu, phi), parse_mathematica(V), modules="math")
func_r = lambdify((a, b, c, d, l, mu, phi), parse_mathematica(r), modules="math")
func_ns = lambdify((a, b, c, d, l, mu, phi), parse_mathematica(ns), modules="math")
func_m32 = lambdify((a, b, c, d, l, mu), parse_mathematica(m32), modules="math")


def process_point(point, *args, **kwargs):
    # sys.stdout = open(os.devnull, "w")
    # null_output = open(os.devnull, "w")
    _point = point.copy()
    _point["mu"] = func_musol(
        _point["a"], _point["b"], _point["c"], _point["d"], _point["l"], _point["phistar"]
    )
    f1 = lambda y3: 1 / math.sqrt(2 * func_Kphiphi(_point["a"], _point["b"], _point["c"], y3))
    f2 = lambda y3: func_dxLnV(
        _point["a"], _point["b"], _point["c"], _point["d"], _point["l"], _point["mu"], y3
    )
    v = lambda y3: func_V(
        _point["a"], _point["b"], _point["c"], _point["d"], _point["l"], _point["mu"], y3
    )

    y0ic = math.sqrt(6) * math.atanh(point["phistar"] / math.sqrt(3 * _point["c"]))

    y1ic = func_dxic(
        _point["a"],
        _point["b"],
        _point["c"],
        _point["d"],
        _point["l"],
        _point["mu"],
        _point["phistar"],
    )

    y2ic = point["phistar"]

    # y0 = x
    # y1 = dx/dN
    # y2 = phi

    def rhs(t, y):
        return [
            y[1],  # y0' =  dx/dN
            -(3 - 1 / 2 * y[1] ** 2) * (y[1] + f2(y[2])),  # y1' = d(dx/dN)/dN
            f1(y[2]) * y[1],  # y2' = dphi/dN
        ]

    # with contextlib.redirect_stdout(null_output):
    r = integrate.ode(rhs)
    r.set_initial_value([y0ic, y1ic, y2ic], 0)
    # r.set_integrator("lsoda", method="adams")
    dt = 1e-1
    y = []
    t = []
    while r.successful() and r.t >= 0 and r.t <= 70:
        try:
            r.integrate(r.t + dt)
            y.append(r.y)
            t.append(r.t)
        except:
            break

    y = np.asarray(y)
    t = np.array(t)

    V_array = np.array([v(y3) for y3 in y[:, 2]])
    eH_array = 1 / 2 * (y[:, 1]) ** 2
    Pr_array = 1 / (8 * math.pi**2) * V_array / (eH_array * (3 - eH_array))

    _point["t"] = t
    _point["x"] = y[:, 0]
    # _point["dx/dN"] = y[:,1]
    # _point["phi"] = y[:,2]
    _point["V"] = V_array
    # _point["eH"] = eH_array
    _point["Pr"] = Pr_array

    _point["smallest_x"] = np.abs(y[:, 0]).min()
    idx_smallest_x = np.argmin(np.abs(y[:, 0]))
    n_smallest_x = t[idx_smallest_x]
    _point["n_smallest_x"] = n_smallest_x

    _point["highest_Pr"] = Pr_array.max()
    idx_highest_Pr = np.argmax(Pr_array)
    n_highest_Pr = t[idx_highest_Pr]
    _point["n_highest_Pr"] = n_highest_Pr

    if t[-1] >= 30:
        _point["highest_Pr_late"] = np.max(Pr_array[t > 30])
        # _idxt30 = np.argmax(_point["t"] >= 30)
        # _point["x_at_30"] = y[_idxt30, 0]
    else:
        _point["highest_Pr_late"] = 1e-20
        # _point["x_at_30"] = y[:, 0].min()
    # idx_highest_Pr = np.argmax(_point["Pr"])
    # n_highest_Pr = _point["t"][idx_highest_Pr]
    # _point["n_highest_Pr"] = n_highest_Pr

    _point["ns"] = func_ns(
        _point["a"],
        _point["b"],
        _point["c"],
        _point["d"],
        _point["l"],
        _point["mu"],
        _point["phistar"],
    )
    _point["r"] = func_r(
        _point["a"],
        _point["b"],
        _point["c"],
        _point["d"],
        _point["l"],
        _point["mu"],
        _point["phistar"],
    )

    _point["m32"] = func_m32(
        _point["a"],
        _point["b"],
        _point["c"],
        _point["d"],
        _point["l"],
        _point["mu"],
    )

    return _point

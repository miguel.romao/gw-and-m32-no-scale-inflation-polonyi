# Gravitational Waves and Gravitino Mass in No-Scale Supergravity Inflation with Polonyi Term

Code repository for the scan and analysis of the paper Gravitational Waves and Gravitino Mass in No-Scale Supergravity Inflation with Polonyi Term.

Below you will find the instructions to reproduce the analysis in the paper. See the paper for more details. Cite the paper if you wish to cite the code herein.

# Files

The number prefix files included loosely follow the logic of the paper.

In `1-SUGRA-Formulae` we can derive the general expressions for the scalar F-term potential and the Kahler metric entries. The scan is performed in `2-Scan-and-GW.nb` which also includes the code to produce the present day gravitational wave energy density spectrum given a specific point of the parameter space.

The jupyter notebooks (`3-ScanAnalysis.ipnb`, `4-PRsr-vs-PR.ipynb`, `5-Sensitivity-Curves.ipynb`) allow to quickly draw plots of the potential, power spectrum, etc of a point.


# Software versions

- `Mathematica 13.2.1.0`
- `python 3.11.5`

The versions of the `python` packages can be seen in the `requirements.txt` file. To reproduce the same `python` environment, one just needs to create a virtual environment and install the dependencies from this file, i.e.

```shell
$ python -m venv venv
$ source venv/bin/activate
(venv) $ pip install -r requirements.txt
```

# Released under MIT Licence

MIT License

Copyright (c) 2023 Authors (Miguel Crispim Romão, Stephen F. King)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


# Other licences and acknowledgements

- The `strain_2_trimmed.png` image of the gravitational wave sensitivity curves for current and near future interferometers was taken from [Kai Schmitz. New Sensitivity Curves for Gravitational-Wave Signals from Cosmological Phase Transitions. JHEP, 01:097, 2021](https://link.springer.com/article/10.1007/JHEP01(2021)097).
- We are immensely grateful to Dr. Ioanna Stamou for her patience in helping us to validate our methodology and to reproduce the results in the literature.